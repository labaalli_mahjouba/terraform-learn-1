terraform {
  /*
   cloud {
        organization = "learning_terraform_labaalli"
        workspaces {
            name = "my_first_workspace"
        }
    }*/
  required_providers {
        aws = {
        source  = "hashicorp/aws"
        version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

resource "aws_instance" "app_server" {
  ami           = "ami-0cf10cdf9fcd62d37"
  instance_type = "t2.micro"
}
